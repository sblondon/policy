#               AUTHORITATIVE LIST OF VIRTUAL PACKAGE NAMES
#
# Below is an authoritative list of virtual package names currently
# in-use or proposed and not objected to.  Please check the list below
# for things relevant to your packages.
#
# New packages MUST use virtual package names where appropriate (this
# includes making new ones - read on).
#
# Packages MUST NOT use virtual package names (except privately, amongst
# a cooperating group of packages) unless they have been agreed upon and
# appear in this list.
#
# The latest version of this file can be found in the debian-policy, or at
# https://www.debian.org/doc/packaging-manuals/virtual-package-names-list.yaml
# (or any other Debian web mirror).
#
# When a virtual package description says "(versioned)", it means that
# binary packages implementing the virtual package are expected to use
# versioned Provides: to declare the API version that is implemented.
#
# For example, "Provides: logind (= 123)" indicates that the package
# provides the same D-Bus APIs that the logind included with systemd
# version 123 provided.
#
# The procedure for updating the list is as follows:
#
# 1. Post to debian-devel saying what names you intend to use or what
#    other changes you wish to make, and file a wish list bug against the
#    package debian-policy.
#
# 2. Wait a few days for comment (some of the comments may be on the
#    debian-policy list, if you are not subscribed, ask for mail to be CC'd
#    to you).
#
# 3. Mail the maintainer of the virtual package name list (which is the
#    Debian Policy list <debian-policy@lists.debian.org>) notifying them
#    of the consensus reached (or your suggestions if no one objected).
#    Please update the bug report at the same time.
#
#    Please include a proposed brief description of the new virtual name(s)
#    for the list.  The list maintainer will then include the updated list
#    in the next release of Policy.
#
# 4. Go and use the new or changed names.

# Miscellaneous

schemaVersion: 1
virtualPackages:
 - name: awk
   description: a suitable /usr/bin/{awk,nawk}
   alternatives:
     - /usr/bin/awk
     - /usr/bin/nawk
 - name: c-shell
   description: a suitable /bin/csh
   alternatives: [/bin/csh]
 - name: dotfile-module
   description: a module for the Dotfile Generator
 - name: emacsen
   description: the GNU emacs or a compatible editor
 - name: lzh-archiver
   description: an LZH archiver package
 - name: tclsh
   description: a /usr/bin/tclsh
   alternatives: [/usr/bin/tclsh]
 - name: wish
   description: a /usr/bin/wish
   alternatives: [/usr/bin/wish]
# Database

 - name: virtual-mysql-client
   description: a MySQL-database-compatible client package
 - name: virtual-mysql-client-core
   description: a MySQL-database-compatible client core package
 - name: virtual-mysql-server
   description: a MySQL-database-compatible server package
 - name: virtual-mysql-server-core
   description: a MySQL-database-compatible server core package
 - name: virtual-mysql-testsuite
   description: a MySQL-database-compatible test suite package

# Development

 - name: c-compiler
   description: a C compiler
 - name: debconf-2.0
   description: the debconf protocol
 - name: fortran77-compiler
   description: a Fortran77 compiler
 - name: kernel-headers
   description: kernel header files (<linux/*.h>, <asm/*.h>)
 - name: kernel-image
   description: kernel image (vmlinuz, System.map, modules)
 - name: kernel-source
   description: kernel source code
 - name: libc-dev
   description: header and object files of 'libc'

# System

 - name: flexmem
   description: anything that can access flexible memory via the OBEX Protocol
 - name: foomatic-data
   description: PPD printer description files
 - name: linux-kernel-log-daemon
   description: a daemon to facilitate logging for the Linux kernel
 - name: system-log-daemon
   description: a daemon that provides a logging facility for other applications
 - name: time-daemon
   description: anything that serves as a time daemon
 - name: ups-monitor
   description: anything that is capable of controlling an UPS
 - name: cron-daemon
   description: Any cron daemon that correctly follows policy requirements
 - name: dbus-session-bus
   description: provides the D-Bus well-known session bus for most or all user login sessions
 - name: default-dbus-session-bus
   description: Debian's preferred implementation of dbus-session-bus, possibly architecture-specific
 - name: dbus-system-bus
   description: provides the D-Bus well-known system bus as a system service, including service activation
 - name: default-dbus-system-bus
   description: Debian's preferred implementation of dbus-system-bus, possibly architecture-specific
 - name: logind
   description: an org.freedesktop.login1 D-Bus API implementation (versioned)
 - name: default-logind
   description: Debian's preferred implementation of logind, possibly architecture-specific (versioned)

# Documentation

 - name: dict-client
   description: clients for the Dictionary Server
 - name: dict-server
   description: the Dictionary Server
 - name: dictd-dictionary
   description: a dictionary for the dictd Dictionary Server
 - name: info-browser
   description: something that can browse GNU Info files
 - name: ispell-dictionary
   description: a dictionary for the ispell system
 - name: myspell-dictionary
   description: a dictionary for the myspell system
 - name: man-browser
   description: something that can read man pages
 - name: stardict-dictionary
   description: a dictionary for stardict
 - name: stardict
   description: application capable of reading stardict-dictdata
 - name: stardict-dictdata
   description: dictionary data which can be read from stardict
 - name: wordlist
   description: a /usr/share/dict/words
   alternatives: [/usr/share/dict/words]
 - name: www-browser
   description: something that can browse HTML files

# Network

 - name: dhcp-client
   description: a DHCP client
 - name: ftp-server
   description: a FTP server
 - name: httpd
   description: a HTTP server
 - name: httpd-cgi
   description: a CGI-capable HTTP server
 - name: httpd-wsgi
   description: a WSGI-capable HTTP server (python 2 API)
 - name: httpd-wsgi3
   description: a WSGI-capable HTTP server (python 3 API)
 - name: ident-server
   description: an identd daemon
 - name: inet-superserver
   description: an inetd server
 - name: lambdamoo-core
   description: a lambdamoo-compatible database package
 - name: lambdamoo-server
   description: anything running a moo using a lambdamoo-core
 - name: radius-server
   description: a RADIUS server for acct/auth
 - name: rsh-client
   description: an rsh client
 - name: rsh-server
   description: an rsh server
 - name: telnet-client
   description: a telnet client
 - name: telnet-server
   description: a telnet server

# News and Mail

 - name: imap-client
   description: a mail reader capable of accessing remote mail folders using the IMAP protocol (e.g. Pine)
 - name: imap-server
   description: an IMAP mail server
 - name: mail-reader
   description: a mail user agent (e.g. Pine, Elm, mailx, &c)
 - name: mail-transport-agent
   description: a mail transport agent (e.g. Smail, Sendmail, &c)
 - name: mailx
   description: a /usr/bin/mailx binary that provides at least the POSIX mailx interface
   alternatives: [/usr/bin/mailx]
 - name: news-reader
   description: a news reader (e.g. trn, tin, &c)
 - name: news-transport-system
   description: a local news system (e.g. INN, C News or B News)
 - name: pgp
   description: a version of PGP (International or US)
 - name: pop3-server
   description: a POP3 Server

# X Window System

 - name: x-display-manager
   description: an X client which manages a collection of X servers
 - name: x-session-manager
   description: a program which starts a desktop environment or launches a general X session
 - name: x-terminal-emulator
   description: an X client which emulates a terminal with a terminfo description in the ncurses-base package
 - name: x-window-manager
   description: an X client which provides window management services
 - name: xserver
   description: an X server that (directly or indirectly) manages physical input and display hardware

# Fonts

 - name: fonts-japanese-gothic
   description: Gothic-style Japanese font
 - name: fonts-japanese-mincho
   description: Mincho-style Japanese font

# Graphics and MultiMedia

 - name: audio-mixer
   description: a utility to control the input and output levels of a sound card, with a tty interface
 - name: x-audio-mixer
   description: a utility to control the input and output levels of a sound card, X Window System interface
 - name: mpd-client
   description: a client that can control the Music Player Daemon
 - name: pdf-preview
   description: a preprocessor that creates PDF output
 - name: pdf-viewer
   description: anything that can display PDF files
 - name: postscript-preview
   description: a preprocessor that creates Postscript output
 - name: postscript-viewer
   description: anything that can display Postscript files

# Java and virtual machines

 - name: java5-runtime
   description: a Java runtime environment, Java version 5
 - name: java6-runtime
   description: a Java runtime environment, Java version 6
 - name: java7-runtime
   description: a Java runtime environment, Java version 7
 - name: java8-runtime
   description: a Java runtime environment, Java version 8
 - name: java9-runtime
   description: a Java runtime environment, Java version 9
 - name: java5-runtime-headless
   description: a non-graphical Java runtime environment, Java ver. 5
 - name: java6-runtime-headless
   description: a non-graphical Java runtime environment, Java ver. 6
 - name: java7-runtime-headless
   description: a non-graphical Java runtime environment, Java ver. 7
 - name: java8-runtime-headless
   description: a non-graphical Java runtime environment, Java ver. 8
 - name: java9-runtime-headless
   description: a non-graphical Java runtime environment, Java ver. 9

# Scheme and interpreters

 - name: scheme-r4rs
   description: Scheme interpreter with the R4RS environment
 - name: scheme-r5rs
   description: Scheme interpreter with the R5RS environment
 - name: scheme-ieee-11878-1900
   description: Scheme interpreter with the IEEE-11878-1900 environment
 - name: scheme-srfi-0
   description: Scheme interpreter accepting the SRFI 0 language extension
 - name: scheme-srfi-7
   description: Scheme interpreter accepting the SRFI 7 language extension
 - name: scheme-srfi-55
   description: Scheme interpreter accepting the SRFI 55 language extension

# Games and Game-related

 - name: adventure
   description: the classic Colossal Cave Adventure game
 - name: doom-engine
   description: an executable Doom engine
 - name: boom-engine
   description: an executable Doom engine supporting the 'boom' feature-set
 - name: doom-wad
   description: the data component of a Doom game, compatible with the original Doom engine
 - name: boom-wad
   description: the data component of a Doom game, using features from the "boom" engine family

# Old and obsolete virtual package names
# --------------------------------------
# Note, that no other package then the ones listed here should use
# these virtual package names.
#
# [There are currently no such package names in use]
#
# Changelog
# ---------
#
# Ian Jackson:
#   22 Sep 1995 Initial revision.
#
# Andrew Howell:
#   26 Mar 1996 Added www-browser.
#
# Manoj Srivastava:
#   11 May 1996 Added kernel-image, added new location of this file
#
# Warwick Harvey:
#   19 May 1996 Took over maintenance of list, changed instructions for
#               updating list
#   25 Jul 1996 Added awk as per Chris Fearnley's suggestion
#               Added c-shell, which seemed to have dropped off at some stage
#    2 Aug 1996 Added pdf-{viewer,preview}, compress, emacs
#    5 Aug 1996 Added imap-{client,server}
#    8 Aug 1996 Added editor
#   20 Aug 1996 Added sgmls, removed metafont, dvilj, dvips
#   25 Nov 1996 Removed editor (should have done this a long time ago)
#
# Christian Schwarz:
#   29 Apr 1997 New maintainer of this list
#    5 May 1997 Added wordlist
#   29 May 1997 Added dotfile-module, ups-monitor, tcl-interpreter,
#               tk-interpreter
#   21 Jun 1997 Removed obsolete virtual packages: xR6shlib, xlibraries,
#               compress, emacs, sgmls, inews, gs_x, gs_svga, gs_both, xpmR6
#               Added new section about obsolete names
#    1 Sep 1997 Renamed 'tcl/tk-interpreter' to 'tclsh/wish'
#   21 Oct 1997 Added emacs, c-compiler, fortran77-compiler, lambdamoo-core,
#               lambdamoo-server
#   29 Jan 1998 Added libc-dev, emacsen
#   14 Apr 1998 Removed obsolete virtual package 'emacs'
#
# Manoj Srivastava:
#   23 Jun 1999 Added pop3-server
#   13 Jul 1999 Added ftp-server
#
# Julian Gilbey:
#   26 Oct 1999 Added ispell-dictionary
#               Added man-browser
#               Added ident-server
#               Alphabeticised lists
#
# Manoj Srivastava:
#   11 Jul 2000 Added x-terminal-emulator
#               Added x-window-manager
#               Added xserver
#               Added linux-kernel-log-daemon
#               Added system-log-faemon
#   24 Aug 2000 Added mp3-encoder
#               Added mp3-decoder
#               Added time-daemon
#               Added rsh-client
#               Added telnet-client
#   16 Jan 2001 Added rsh server
#               Added telnet-server
#
# Julian Gilbey:
#   13 Feb 2001 Removed libc.4.so
#               Removed xcompat virtual package names
#
# Manoj Srivastava:
#   14 Mar 2002 Added java-compiler, java2-compiler
#               Added java-virtual-machine
#               Added java1-runtime and java2-runtime
#               Added dict-client
#               Added foomatic-data
#               Added audio-mixer and x-audio-mixer
#   30 Aug 2002 Added debconf-2.0
#               Added dhcp-client
#               Added aspell-dictionary
#               Added radius-server
#    9 Sep 2002 Added dict-server
#    3 Aug 2003 Added myspell-dictionary
#
# Andreas Barth:
#   25 Apr 2004 Added stardict-dictionary
#               Added inetd-superserver
#
# Manoj Srivastava:
#   25 Jun 2004 Added cron-daemon
#
# Manoj Srivastava:
#    4 Feb 2005 Added mpd-client
#               Added flexmem
#
# Manoj Srivastava:
#    16 Jun 2005  Removed aspell-dictionary
#
# Manoj Srivastava:
#    18 Jun 2005  Added x-session-manager
#                 Added the section on Scheme and interpreters, which
#                 includes:
#                   scheme-r4rs
#                   scheme-r5rs
#                   scheme-ieee-11878-1900
#                   scheme-srfi-0
#                   scheme-srfi-7
#                   scheme-srfi-55
#                 Added x-display-manager
#
# Manoj Srivastava:
#    26 April 2006 Added httpd-cgi
#
# Manoj Srivastava:
#    02 October 2006 Added stardict
#                    Added stardict-dictdata
#                    Added lzh-archiver
#
# Russ Allbery:
#    8 Jul 2007 Added dictd-dictionary
#               Rename inetd-superserver to inet-superserver
#    2 Dec 2007 Added ttf-japanese-gothic
#               Added ttf-japanese-mincho
#
# Manoj Srivastava:
#   21 Nov 2009 (Re)Added cron-daemon
#
# Russ Allbery:
#   27 Aug 2010 Added mailx
#
# Bill Allombert:
#   22 Feb 2012 Rename ttf-japanese-gothic to fonts-japanese-gothic
#               Rename ttf-japanese-mincho to fonts-japanese-mincho
#               Removed java-compiler
#               Removed java2-compiler
#               Removed java-virtual-machine
#
# Charles Plessy:
#   03 Aug 2013 Removed mp3-encoder
#   17 Aug 2013 Removed mp3-decoder
#
# Bill Allombert:
#   16 Jul 2014 Added java{5,6,7,8,9}-runtime{,-headless}
#               Removed java1-runtime, java2-runtime
#   30 Jul 2014 Added httpd-wsgi
#
# Russ Allbery:
#   01 Jan 2017 Added httpd-wsgi3
#               Added MySQL virtual packages:
#                 virtual-mysql-client
#                 virtual-mysql-client-core
#                 virtual-mysql-server
#                 virtual-mysql-server-core
#                 virtual-mysql-testsuite
#   08 Jan 2017 Added adventure
#
# Sean Whitton:
#   23 Dec 2018 Added dbus-session-bus
#               Added default-dbus-session-bus
#   15 Feb 2019 Added logind
#               Added default-logind
#   29 Jan 2022 Added dbus-system-bus
#               Added default-dbus-system-bus
